import java.util.*;
import java.io.*;
import java.net.*;
import java.rmi.*;
import java.rmi.server.*;

public class RMIChatServerImpl extends UnicastRemoteObject
				implements RMIChatServer {

	/**
	 *  The client vector. This holds one entry
	 *  for each client that has registered with
	 *  the server.
	 */
	private Vector myClientVector;

	/**
	 *  Constructs the chat server
	 */
	public RMIChatServerImpl() throws RemoteException {

		// simply instantiate the Vector
		myClientVector = new Vector();

	}

	/**
	 *  Allows the registration of a chat client
	 */
	public void connect(RMIChatClient theClient) throws RemoteException {



		// store the reference
		myClientVector.addElement(theClient);

		System.out.println("Bound Client: " + theClient);
	}

	/**
	 *  Allows the sending of a message
	 */
	public void sendMessageToServer( String theMessage)
	  throws RemoteException {



		RMIChatClient tempClient;

		// build the message to send
		String outputMessage = "Message received : " + theMessage;
		System.out.println("Sending: " + outputMessage);

		// get an iterator for the vector
		Iterator it = myClientVector.iterator();

		// iterate over the entries
		while (it.hasNext()){

			// send the message
			tempClient = (RMIChatClient)it.next();
			try {
				tempClient.dispatchMessage( outputMessage);
			}
			catch (RemoteException re) {
				// failed to send to client, log it and carry on
				// most likely reason is that client has
				// gone away but we do not know about it
				System.out.println("Failed to send to: " + tempClient + " reason: " + re);
			}
		}

	}

	public static void main (String[] args){

		try{


			RMIChatServer myObject = new RMIChatServerImpl();

		 	Naming.rebind("ServerName123", myObject);

		 	System.out.println("Remote object bound to registry");
		}
		catch( Exception e){

			System.out.println("Failed to register object " + e);
			e.printStackTrace();
			System.exit(1);

		}

	}

}