import java.rmi.*;
import java.util.*;

public interface RMIChatClient extends Remote {

	/**
	 *  allows the sending of a message to the client
	 */
	public void dispatchMessage(String theMessage) throws RemoteException;

}