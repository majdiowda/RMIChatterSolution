import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.naming.*;
import java.rmi.*;

public class Chatter extends JFrame {

	/**
	 * Reference to the Chat Server
	 */
	private RMIChatServer chatServer;

	/**
	 *  The current received message display
	 */
	private JTextArea receivedList;

	/**
	 *  Send button
	 */
	private JButton sendButton;

	/**
	 *  Connect button
	 */
	private JButton connectButton;

	/**
	 *  The current message
	 *  line to be sent
	 */
	private JTextField messageText;

	/**
	 *  This constructor creates the Chatter UI
	 */
	public Chatter() {

		//
		//  LAYOUT FRAME
		//
		setTitle("Chatter");

		Container container = this.getContentPane();
		container.setLayout(new BorderLayout());

		receivedList = new JTextArea(20,30);
		messageText = new JTextField(30);

		sendButton = new JButton("Send");
		sendButton.setEnabled(false);
		connectButton = new JButton("Connect...");

		// layout the UI
		Panel bottomPanel = new Panel();
		bottomPanel.add(connectButton);
		bottomPanel.add(new JLabel("Enter Message"));
		bottomPanel.add(messageText);
		bottomPanel.add(sendButton);

		container.add(bottomPanel, BorderLayout.SOUTH);
		container.add(receivedList, BorderLayout.CENTER);

		pack();

		//
		//  ADD LISTENERS
		//
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				setVisible(false);
				dispose();
				System.exit(0);
		}});

		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				connectToServer();
		}});

		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				sendToServer( messageText.getText());
		}});

	}


	/**
	 *  Connects to the chat server
	 */
	public void connectToServer(){

		// ask the user for a server name
		String bindName = JOptionPane.showInputDialog(this,
							"Please give server name",
							"Chatter",
							JOptionPane.QUESTION_MESSAGE);


		try{


			System.out.println("Looking up " + bindName);

			// get a reference to the remote object
			chatServer = (RMIChatServer) Naming.lookup(bindName);

			// connect to the server
			RMIChatClient myClient = new RMIChatClientImpl(receivedList);
			chatServer.connect(myClient);

			// allow messages to be sent
			sendButton.setEnabled(true);

			System.out.println("Connected!");
		}
		catch(Exception e){
			log("Failed to find remote class" + e);
			JOptionPane.showMessageDialog(this,
										"Failed to find server: " + bindName,
										"Chatter",
										JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 *  Takes the entered message and
	 *  sends it to the chat server
	 */
	public void sendToServer( String theMessage){


		try{
			// send the message
			chatServer.sendMessageToServer( theMessage);

		}
		catch(Exception e){
			log("Failed to send message" + e);
			e.printStackTrace();
		}

		// clear the data item
		messageText.setText("");

	}

	/**
	 *	Convience method for printing system messages
	 */
	 public void log (String message) {

		 System.out.println("Chatter: " + message);
	 }

	/**
	 *  Main method to create the Chatter program and display the GUI frame.
	 */
	public static void main(String[] args)  {

		Chatter myFrame = new Chatter();

		myFrame.setVisible(true);
	}

}