import java.util.*;
import java.io.*;
import java.net.*;
import java.rmi.*;
import java.rmi.server.*;
import javax.swing.*;

public class RMIChatClientImpl extends UnicastRemoteObject implements RMIChatClient {

	/** The reference to the JList to
	 * be supplied with messages
	 */
	 JTextArea theSink;

	/**
	 *  Constructs the chat client
	 */
	public RMIChatClientImpl( JTextArea theSuppliedSink) throws RemoteException {

		// simply store the reference
		theSink = theSuppliedSink;

	}

	/**
	 *  Allows the sending of a message
	 */
	public void dispatchMessage( String theMessage) throws RemoteException {



		// add the message to the vector
		theSink.append("\n" + theMessage);

	}

}