import java.rmi.*;
import java.util.*;
public interface RMIChatServer extends Remote {

	/**
	 *  Allows the registration of a chat client
	 */
	public void connect(RMIChatClient theClient) throws RemoteException;

	/**
	 *  allows the sending of a message to the server
	 */
	public void sendMessageToServer(String theMessage) throws RemoteException;

}